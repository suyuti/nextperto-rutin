const moment = require('moment')

exports.getGecikmisGorelerMessage = (user, gorevler, ekipGorevleri) => {
    var message = `<p>Sayın ${user.username},</p>
    <p></p>
    <p>Son günü bugün olan görevleriniz aşağıda listelenmiştir.</p>
    <br />
    
    `
    var gorevlerTable = `    <table style="width: 100%; border-collapse: collapse;">
    <colgroup>
       <col span="1" style="width: 30%;">
       <col span="1" style="width: 10%;">
       <col span="1" style="width: 10%;">
       <col span="1" style="width: 10%;">
       <col span="1" style="width: 10%;">
       <col span="1" style="width: 10%;">
       <col span="1" style="width: 10%;">
    </colgroup>
    
    <thead>
        <tr>
            <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Görev</td>
            <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Müşteri</td>
            <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Son Tarih</td>
            <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Oluşturma Tarihi</td>
            <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Görevi oluşturan</td>
            <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Öncelik</td>
        </tr>
    </thead>
    <tbody>
    `

    for (const gorev of gorevler) {
        gorevlerTable +=
            `            
        <tr style="border-top: thin solid darkgray">
        <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${gorev.baslik}</td>
        <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${gorev.firma ? gorev.firma.marka : 'İç Görev'}</td>
        <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${moment(gorev.sonTarih).format('DD.MM.YYYY')}</td>
        <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${moment(gorev.createdAt).format('DD.MM.YYYY')}</td>
        <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${gorev.createdBy ? gorev.createdBy.username: ''}</td>
        <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${gorev.oncelik}</td>
    </tr>
    `
    }
    gorevlerTable += `</tbody>
    </table>`
    message += gorevlerTable

    
 
    let i = 0
    for (let eGorevler of ekipGorevleri) {
        if (eGorevler.gorevler.length == 0) {
            continue
        }
        if (i == 0) {
            if (ekipGorevleri.length > 0) {
                message += `<br/>
                    <h4>Ekibimin bugün tarihli görevleri</h4>
                `
            }
        }
        i++
        
        message += `<p>${eGorevler.username} gorevleri</p>`

        var eGorevlerTable = `    <table style="width: 100%; border-collapse: collapse;">
        <colgroup>
           <col span="1" style="width: 30%;">
           <col span="1" style="width: 10%;">
           <col span="1" style="width: 10%;">
           <col span="1" style="width: 10%;">
           <col span="1" style="width: 10%;">
           <col span="1" style="width: 10%;">
           <col span="1" style="width: 10%;">
        </colgroup>
        
        <thead>
            <tr>
                <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Görev</td>
                <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Müşteri</td>
                <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Son Tarih</td>
                <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Oluşturma Tarihi</td>
                <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Görevi oluşturan</td>
                <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Öncelik</td>
            </tr>
        </thead>
        <tbody>
        `

        for (const gorev of eGorevler.gorevler) {
            eGorevlerTable +=
                `            
            <tr style="border-top: thin solid darkgray">
            <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${gorev.baslik}</td>
            <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${gorev.firma ? gorev.firma.marka : 'İç Görev'}</td>
            <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${moment(gorev.sonTarih).format('DD.MM.YYYY')}</td>
            <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${moment(gorev.createdAt).format('DD.MM.YYYY')}</td>
            <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${gorev.createdBy ? gorev.createdBy.username : ''}</td>
            <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${gorev.oncelik}</td>
        </tr>
        `
        }
        eGorevlerTable += `</tbody>
        </table>`
        message += eGorevlerTable
    }

    return message

}