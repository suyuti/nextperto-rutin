const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key         : String,
    active      : Boolean,
    adi         : String,
    vyonetici   : String,
    vuyeler     : [String]
}, {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
})

SchemaModel.virtual('yonetici', {
    ref         : 'User',
    localField  : 'vyonetici',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('uyeler', {
    ref         : 'User',
    localField  : 'vuyeler',
    foreignField: 'key'
})

module.exports = mongoose.models.Ekip || mongoose.model("Ekip", SchemaModel);
