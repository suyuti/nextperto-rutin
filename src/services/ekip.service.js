const MongooseQueryParser = require('mongoose-query-parser')
const Ekip = require('../models/ekip.model')
const moment=require('moment')
const _ = require('lodash')

const getAllChildren = (ekipler, user) => {
    let result = []
    let yoneticiOlduguEkipler = ekipler.filter(e => e.vyonetici == user)
    if (yoneticiOlduguEkipler.length == 0) {
        return null
    }
    for (let ekip of yoneticiOlduguEkipler) {
        result = _.concat(result, ekip.vuyeler)
        for (let user of ekip.vuyeler) {
            let r = getAllChildren(ekipler, user)
            if (r) {
                result = _.concat(result, r)
            }
        }
    } 
    return result
}

exports.getEkibim = async (user) => {

    //let yoneticisiOldugumEkipler    = await Ekip.find({vyonetici: user.key, active:true}).exec()
    //let dahilOldugumEkipler         = await Ekip.find({vuyeler: user.key, active: true}).exec()
    let tumEkipler                  = await Ekip.find({active:true}).exec()

    let result = _.uniq(getAllChildren(tumEkipler, user.key))
    return result
}
