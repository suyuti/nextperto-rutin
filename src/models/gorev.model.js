const mongoose = require('mongoose');
//var diffHistory = require('mongoose-diff-history/diffHistory')
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key             : String,  
    baslik          : {type: String, required: true},
    aciklama        : String,
    sonTarih        : Date,
    icGorevMi       : Boolean,
    oncelik         : String,
    sonuc           : String,
    sonucAciklama   : String,
    vfirma          : String,
    //firma           : {type: mongoose.Schema.Types.ObjectId, ref: 'Musteri'},
    vsorumlu        : String,
    //sorumlu         : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    vcreatedBy      : String,
    //createdBy       : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    createdAt       : { type: Date },
    vkapatan        : { type: String }, 
    //kapatan         : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    kapatmaTarihi   : { type: Date },
    vtoplanti       : String,
    //toplanti        : { type: mongoose.Schema.Types.ObjectId, ref: 'Toplanti'}, // Toplanti kararindan olusturlmussa ilgili toplanti
    vtoplantiKarar  : String,
    //toplantiKararId : { type: mongoose.Schema.Types.ObjectId, ref: 'ToplantiKarar'} // Toplanti kararindan olusturlmussa ilgili toplanti
    kategori        : String
},
{
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
})

//SchemaModel.plugin(diffHistory.plugin)

SchemaModel.virtual('firma', {
    ref         : 'Firma',
    localField  : 'vfirma',
    foreignField: 'key',
    justOne     : true
})
SchemaModel.virtual('sorumlu', {
    ref         : 'User',
    localField  : 'vsorumlu',
    foreignField: 'key',
    justOne     : true
})
SchemaModel.virtual('createdBy', {
    ref         : 'User',
    localField  : 'vcreatedBy',
    foreignField: 'key',
    justOne     : true
})
SchemaModel.virtual('kapatan', {
    ref         : 'User',
    localField  : 'vkapatan',
    foreignField: 'key',
    justOne     : true
})
SchemaModel.virtual('toplanti', {
    ref         : 'Toplanti',
    localField  : 'vtoplanti',
    foreignField: 'key',
    justOne     : true
})
SchemaModel.virtual('toplantiKarar', {
    ref         : 'ToplantiKarar',
    localField  : 'vtoplantiKarar',
    foreignField: 'key',
    justOne     : true
})


module.exports = mongoose.models.Gorev || mongoose.model("Gorev", SchemaModel);
