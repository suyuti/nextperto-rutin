const GorevService = require('../services/gorev.service')
const EkipService = require('../services/ekip.service')
const FirmaModel = require('../models/firma.model')
const UserModel = require('../models/user.model')
const GorevHelper = require('../helpers/gorev.helper')
const AmqpProvider = require('../providers/amqp.provider')

const moment = require('moment')
const fs = require('fs')

exports.sendGecikmisGorevlerMail = async () => {
    // Kullanicilari al
    // Geciken gorevlerini bul
    // Ekipte yonetici ise ekibinin geciken gorevlerini bul
    // kendisine (kendi gorevleri + ekibinin gorevleri) mail gonder

    let users = await UserModel.find({ active: true }).lean(true).exec()

    let today = moment().set('hour', 0).set('minute', 0).set('second', 0).set('millisecond', 0)
    let todayPlus1 = moment().add(1, 'day').set('hour', 0).set('minute', 0).set('second', 0).set('millisecond', 0)

    for (let user of users) {
        let predefined = {
            user: {vsorumlu: user.key},
            tarih: {
                $and: [
                    {sonTarih: {$gte: today.toISOString(true)}},
                    {sonTarih: {$lt: todayPlus1.toISOString(true)}}
                ]
            }
        }
        //let kendiGecikenGorevler = await GorevService.getGorevler(`vsorumlu=${user.key}&sonuc=acik&sonTarih=${moment().toISOString()}&populate=firma,createdBy`)
        let kendiGecikenGorevler = await GorevService.getGorevler('${tarih}&${user}&sonuc=acik&populate=firma,createdBy', predefined)
        let ekip = await EkipService.getEkibim(user)
        let ekibinGecikenGorevleri = []
        for (let pers of ekip) {
            let persUser = await UserModel.findOne({ key: pers }).lean(true).exec()
            if (!persUser) {
                continue
            }
            let _predefined = {
                user: {vsorumlu: pers},
                tarih: {
                    $and: [
                        {sonTarih: {$gte: today.toISOString(true)}},
                        {sonTarih: {$lt: todayPlus1.toISOString(true)}}
                    ]
                }
            }
    
            let perGecikenGorevler = await GorevService.getGorevler('${tarih}&${user}&sonuc=acik&populate=firma,createdBy', _predefined)
            ekibinGecikenGorevleri.push({ username: persUser.username, gorevler: perGecikenGorevler })
        }
        if (kendiGecikenGorevler.length > 0 || ekibinGecikenGorevleri.length > 0) {
            let m = GorevHelper.getGecikmisGorelerMessage(user, kendiGecikenGorevler, ekibinGecikenGorevleri)
            await AmqpProvider.sendMail({
                from:'5b487f55-8501-4b6f-9297-f025cd56e1e1', // Experto APP
                to: ['mehmet.dindar@experto.com.tr'],
                //to: [user.email],
                subject: `[EXPERTO-APP] ${user.username} Son günü bugün olan görevler var.`,
                content: m
            })
            console.log(`${user.email} kendi: ${kendiGecikenGorevler.length} ekibi: ${ekibinGecikenGorevleri.length}`)
            //fs.writeFileSync(`${user.key}_mail.html`, m)
        }
    }
}

exports.sendSongunBugunGorevlerMail = async () => {
    let gecikenGorevler = await GorevService.getGecikenAcikGorevler()
    console.log(JSON.stringify(gecikenGorevler, null, 2))
}
