const mongoose = require('mongoose');
var diffHistory = require('mongoose-diff-history/diffHistory')
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key                     : { type: String},
    baslik                  : {type: String, required: true},
    aciklama                : String, 
    start                   : Date,
    end                     : Date,

    tarih                   : Date,
    saat                    : String,
    sure                    : Number,

    yer                     : String,
    tur                     : String,
    kategori                : String,
    gundem                  : String,

    vfirma                  : String,
    //firma                   : {type: mongoose.Schema.Types.ObjectId, ref:'Musteri'},
    vfirmaKatilimcilar      : [String],
    //firmaKatilimcilar       : [{type: mongoose.Schema.Types.ObjectId, ref: 'Kisi'}],
    
    vorganizerKatilimcilar  : [String],
    //organizerKatilimcilar   : [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    
    vcreatedBy              : String,
    //createdBy               : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    createdAt               : Date,
    
    vupdatedBy              : String,
    //updatedBy               : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    updatedAt               : Date,
    //tamamlandi              : Boolean,
    status                  : { type: String, enum: ['acik', 'tamamlandi', 'iptal'], default: 'acik'},
    
    vkapatan                : String,
    //kapatan                 : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    kapatmaTarihi           : Date,
    kapatmaAciklama         : String,
    msEventId               : String, // Microsoft event Id.
    icToplanti              : { type: Boolean, default: false }
},
{
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
})

SchemaModel.plugin(diffHistory.plugin)

SchemaModel.virtual('firma', {
    ref         : 'Firma',
    localField  : 'vfirma',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('createdBy', {
    ref         : 'User',
    localField  : 'vcreatedBy',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('updatedBy', {
    ref         : 'User',
    localField  : 'vupdatedBy',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('kapatan', {
    ref         : 'User',
    localField  : 'vkapatan',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('firmaKatilimcilar', {
    ref         : 'Kisi',
    localField  : 'vfirmaKatilimcilar',
    foreignField: 'key',
})

SchemaModel.virtual('organizerKatilimcilar', {
    ref         : 'User',
    localField  : 'vorganizerKatilimcilar',
    foreignField: 'key',
})

module.exports = mongoose.models.Toplanti || mongoose.model("Toplanti", SchemaModel);
