// Firmalarin bazi bilgileri sirkette elle duzeltildi. DB'de bir defalik guncelleme yapialcak
const FirmaModel = require('./models/firma.model')
const fs = require('fs')
let guncellemeExcel = './FirmaListesi_20210216.csv'
const initMongo = require("./helpers/mongo");

const updateFirma = async () => {
    try {
        await initMongo.connect()
        console.log('baglandi')
        let csv = fs.readFileSync(guncellemeExcel, 'utf-8')
        let lines = csv.split(/\r?\n/)
        for (let i in lines) {
            let line    = lines[i].split('|')
            let kobi    = line[2].toUpperCase() == 'X'
            let sanayi  = line[3].toUpperCase() == 'X'
            let aktif   = line[4].toUpperCase() == 'X'
            let pasif   = line[5].toUpperCase() == 'X'

            let query = {
                kobiMi: true
            }

            if (kobi && sanayi) {
                console.log(`${i} ${line[1]} Hem kobi hem sanayi`)
                break
            }
            if (aktif && pasif) {
                console.log(`${i} ${line[1]} Hem aktif hem pasif`)
                break
            }


            //if (kobi || sanayi) {
                if (kobi) {
                    query.kobiMi = true
                }
                else if (sanayi) {
                    query.kobiMi = false
                }
            //}

            //if (aktif || pasif) {
                if (aktif) {
                    query.durum = 'aktif'
                }
                else if (pasif) {
                    query.durum = 'pasif'
                }
                else {
                    query.durum='potansiyel'
                }
            //}

            //console.log(`${line[1]} ${aktif} ${pasif}`)
            //console.log(query)

            //if (!kobi && !sanayi && !aktif && !pasif) {
            //   // console.log(`${i} ${kobi} ${sanayi} ${aktif} ${pasif}`)
            //    continue
            //}

            let firma = await FirmaModel.findOneAndUpdate({ key: line[0] }, query, { new: true, lean: true })
            if (!firma) {
                continue
            }
            console.log(`${i} ${firma.marka} ${firma.kobiMi?'kobi':'sanayi'} ${firma.durum}`)
        }
        await initMongo.disconnect()
    }
    catch (e) {
        console.log(e)
    }
}

updateFirma()