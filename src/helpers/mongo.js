const mongoose = require('mongoose')
const MONGO_URI = process.env.MONGO_URI;

console.log(MONGO_URI)
const connect = async (uri='') => {
    return mongoose.connect(MONGO_URI||uri, {
        keepAlive: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    })
    //.then(() => {
    //    console.log('DB OK')
    //}, err => {
    //    console.log(`DB error `, err)
    //})
}

const disconnect = () => {
    mongoose.disconnect().then(() => {
        console.log('DB Closed')
    })
}

module.exports = {
    connect,
    disconnect
}

