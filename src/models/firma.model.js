//const Sektor = require('./sektor.model')
const mongoose = require('mongoose');
//var diffHistory = require('mongoose-diff-history/diffHistory')
const Schema = mongoose.Schema;

const musteriDurumlari = ['aktif', 'pasif', 'potansiyel', 'sorunlu']

var SchemaModel = new Schema({
    key             : { type: String},
    vtenants        : [String],
    marka           : { type: String, required: true},
    unvan           : { type: String, required: true},
    //sektor          : { type: mongoose.Schema.Types.ObjectId, ref: 'Sektor'},
    domain          : { type: String },
    KepAdresi       : { type: String },
    vergiDairesi    : { type: String},
    vergiNo         : { type: String},
    eFaturaMi       : { type: Boolean, default: false},
    ciro            : { type: Number},
    kobiMi          : { type: Boolean, default: true},
    durum           : { type: String, enum: musteriDurumlari, default: 'potansiyel'},
    vsektor         : { type: String},
    il              : String,
    ilce            : String,
    ulke            : String,
    adres           : String,
    vade            : { type: Number}, // Firma ile calisilan vade
    fabrikaAdres    : String,
    faturaAdres     : String,
    telefon         : String,
    webAdresi       : String,
    vsorumlu        : String,
    argeMerkezi     : Boolean,
    tasarimMerkezi  : Boolean,
    teknoparkFirmasi: Boolean,
    faal            : Boolean,
    oldId           : String
}, {
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
})

//SchemaModel.plugin(diffHistory.plugin)

SchemaModel.virtual('sektor', {
    ref         : 'Sektor',
    localField  : 'vsektor',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('sorumlu', {
    ref         : 'User',
    localField  : 'vsorumlu',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('tenants', {
    ref         : 'Tenant',
    localField  : 'vtenants',
    foreignField: 'key'
})

module.exports = mongoose.models.Firma || mongoose.model("Firma", SchemaModel);
