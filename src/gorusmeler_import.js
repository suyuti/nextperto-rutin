const GorusmeModel = require('./models/gorusme.model')
const FirmaModel = require('./models/firma.model')
const KisiModel = require('./models/kisi.model')
const UserModel = require('./models/user.model')

//const fs = require('fs')
let gorusmeler = require('../gorusmeler.json')
const initMongo = require("./helpers/mongo");
const moment = require('moment')

/*

    key         : String,
    not         : String,
    tarih       : Date,
    saat        : String,
    vfirma      : String,
    vkisi       : String,
    kanal       : String,
    icGorusmeMi : Boolean,
    vcreatedBy  : String,
    createdAt   : Date

    "_id":{"$oid":"5f688bde173fb01444cdb8dc"},
    "konu":"Firmaya ulaşılamadı. Esra Hanıma ödeme hakkında mail atıldı.",
    "tarih":"2020-09-21T11:08:42.856Z",
    "gorusmeYapanMsId":"3fa1c6da-9f42-44e8-9a0c-9bc09c904002",
    "musteri":{"$oid":"5f5a10ea18ed139814dfe9a6"},
    "kisi":{"$oid":"5f5a10ea18ed139814dfe9a7"},
    "saat":"13:30",
    "kanal":"Mail",
    "createdAt":{"$date":{"$numberLong":"1600687070101"}},
    "updatedAt":{"$date":{"$numberLong":"1600687070101"}}

*/


const importGorusmeler = async () => {
    try {
        await initMongo.connect()

        let firmalar = await FirmaModel.find().exec()
        let kisiler = await KisiModel.find().exec()
        let users = await UserModel.find().exec()

        for (let gorusme of gorusmeler) {
            //console.log(gorusme.createdAt.$date.$numberLong)

            let firma = firmalar.find(f => f.oldId == gorusme.musteri.$oid)
            if (!firma) {
                continue
            }

            let kisi = kisiler.find(k => k.oldId == gorusme.kisi.$oid)
            if (!kisi) {
                continue
            }

            console.log(gorusme.gorusmeYapanMsId)
            let user = users.find(u => u.oid == gorusme.gorusmeYapanMsId)
            if (!user) {
                continue
            }

            let g = {
                key: '',
                not: gorusme.konu,
                tarih: gorusme.tarih,
                saat: gorusme.saat,
                vfirma      : firma.key,
                vkisi       : kisi.key,
                kanal: gorusme.kanal,
                icGorusmeMi : false,
                vcreatedBy  : user.key,
                createdAt: new moment.unix(gorusme.createdAt.$date.$numberLong)//.format('dddd, MMMM Do, YYYY h:mm:ss A')
            }
            console.log(g)



            await new GorusmeModel(g).save()
        }
        await initMongo.disconnect()
    }
    catch (e) {
        console.log(e)
    }
}

importGorusmeler()
