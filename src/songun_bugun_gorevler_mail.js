const GecikmisGorevlerController = require('./controllers/gecikmis_gorevler_mail.controller')
const initMongo = require("./helpers/mongo");

const progress = async () => {
    await initMongo.connect()
    await GecikmisGorevlerController.sendGecikmisGorevlerMail()
    await initMongo.disconnect()

}
progress()