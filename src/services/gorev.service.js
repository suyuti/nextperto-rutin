const MongooseQueryParser = require('mongoose-query-parser')
const GorevModel = require('../models/gorev.model')
const moment = require('moment')

exports.getGorevler = async (query, predefined) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter, predefined)
        let gorevler = await GorevModel
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .sort(f.sort)
            .lean(true)
            .exec();
        return gorevler
    }
    catch(e) {
        console.log(e)
    }
}

exports._getGorevler = async (query) => {
    //let today = new Date()
    //today.setHours(0, 0, 0, 0)

    let gorevler = await GorevModel.aggregate(
        [
            {
                '$match': query
                //{
                //  'sonuc': 'acik',
                //  'sonTarih': {'$lt': day}
                //}
            }, {
                '$lookup': {
                    'from': 'firmas',
                    'localField': 'vfirma',
                    'foreignField': 'key',
                    'as': 'firma'
                }
            }, {
                '$project': {
                    'baslik': 1,
                    'firma': {
                        '$arrayElemAt': [
                            '$firma', 0
                        ]
                    },
                    'aciklama': 1,
                    'vsorumlu': 1,
                    'key': 1,
                    'sonTarih': 1,
                    'oncelik': 1,
                    'createdAt': 1
                }
            }, {
                '$group': {
                    '_id': '$vsorumlu',
                    'gorevler': {
                        '$push': '$$ROOT'
                    },
                    'adet': {
                        '$sum': 1
                    }
                }
            }, {
                '$lookup': {
                    'from': 'users',
                    'localField': '_id',
                    'foreignField': 'key',
                    'as': 'sorumlu'
                }
            }, {
                '$project': {
                    'gorevler': 1,
                    'adet': 1,
                    'sorumlu': {
                        '$arrayElemAt': [
                            '$sorumlu', 0
                        ]
                    }
                }
            }
        ]
    )
        .exec()
    return gorevler
}

exports.getGorevMailContent = (item) => {
    var message = `<p>Sayın ${item.sorumlu.username},</p>
<p></p>
<p>Geciken görevleriniz aşağıda listelenmiştir.</p>
<br />

`
    var gorevlerTable = `    <table style="width: 100%; border-collapse: collapse;">
<colgroup>
   <col span="1" style="width: 30%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
</colgroup>

<thead>
    <tr>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Görev</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Müşteri</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Son Tarih</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Oluşturma Tarihi</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Gecikme</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Görevi oluşturan</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Öncelik</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Kategori</td>
    </tr>
</thead>
<tbody>
`

    for (const gorev of item.gorevler) {
        //console.log(gorev)
        gorevlerTable +=
            `            
    <tr style="border-top: thin solid darkgray">
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${gorev.baslik}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${gorev.musteri ? gorev.musteri.marka : 'İç Görev'}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${moment(gorev.sonTarih).format('DD.MM.YYYY')}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${moment(gorev.createdAt).format('DD.MM.YYYY')}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${moment(new Date()).diff(gorev.sonTarih, 'days')} gün</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${gorev.olusturanPersonel.displayName}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${gorev.oncelik}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${gorev.gorevKategori}</td>
</tr>
`
    }
    gorevlerTable += `</tbody>
</table>`
    message += gorevlerTable

    //console.log(message)

}