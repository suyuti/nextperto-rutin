const GelenFaturaModel = require('./src/models/gelenFatura.model')
const TenantModel = require('./src/models/tenant.model')
const TedarikciModel = require('./src/models/tedarikci.model')
const soap = require('soap')
const fs = require('fs')
var AdmZip = require('adm-zip');
const Xml = require('xml2js')
const { uuid } = require('uuidv4');
const initMongo = require("./src/helpers/mongo");

const URL = 'https://efaturaconnector.efinans.com.tr/connector/ws/connectorService?wsdl'
const USER = 'munire.elibologluArge'
const PWD = 'Argeevi456'

const CreateQnbClient = ({ url, user, pwd }) => {
    return new Promise((resolve, reject) => {
        const wsSecurity = new soap.WSSecurity(user, pwd, {})
        soap.createClient(url, (err, client) => {
            if (err) {
                reject(`soap createClient error: ${err}`)
            }
            else {
                client.setSecurity(wsSecurity)
                console.log('QNB Login OK')
                resolve(client)
            }
        })
    })
}

const GelenBelgeleriSorgula = async (client, { vkn, sonAlinanBelgeSiraNo, belgeTuru }) => {
    client.gelenBelgeleriAl({
        vergiTcKimlikNo: vkn,
        sonAlinanBelgeSiraNumarasi: sonAlinanBelgeSiraNo,
        belgeTuru: belgeTuru
    }, (err, res) => {
        if (err) {
            console.log(err)
        }
        else {
            console.log(res.return)
        }
    })
}

const GelenBelgeleriAl = (client, { vkn, sonAlinanBelgeSiraNo, belgeTuru }) => {
    return new Promise((resolve, reject) => {
        client.gelenBelgeleriAl({
            vergiTcKimlikNo: vkn,
            sonAlinanBelgeSiraNumarasi: sonAlinanBelgeSiraNo,
            belgeTuru: belgeTuru
        }, (err, res) => {
            if (err) {
                console.log(err)
                reject(err)
            }
            else {
                if (res == null) {
                    resolve([])
                }
                else {
                    resolve(res.return)
                }
            }
        })
    })
}

const GelenBelgeleriIndir = (client, { vergiTcKimlikNo, ettnler, belgeTuru, belgeFormati }) => {
    return new Promise((resolve, reject) => {
        client.gelenBelgeleriIndir({ vergiTcKimlikNo, ettnler, belgeTuru, belgeFormati }, (err, res) => {
            if (err) {
                console.log(err)
                reject(err)
            }
            else {
                //console.log(res)
                resolve(res.return)
            }
        })
    })
}


const Process = async ({ tenantKey }) => {
    //await initMongo.connect('mongodb://experto:suyuti9419@161.35.143.57:27017/ExpertoDb')

    let tenant = await TenantModel.findOne({ key: tenantKey }).lean(true).exec()
    if (!tenant) {
        console.log(`${tenantKey} tenant bulunamadi`)
        //  initMongo.disconnect()
        return
    }
    let maxBelge = await GelenFaturaModel.findOne({ tenantKey: tenant.key }).sort({ belgeSiraNo: -1 }).exec()

    let client = await CreateQnbClient({ url: tenant.efaturaUrl, user: tenant.efaturaUser, pwd: tenant.efaturaPwd })
    if (!client) {
        console.log('Client NULL')
        return
    }

    let sonAlinanBelgeSiraNo = maxBelge ? maxBelge.belgeSiraNo : 0

    do {
        // 1. gelen belgeleri Al
        let gelenBelgeler = await GelenBelgeleriAl(client, {
            vkn: tenant.vkn,
            sonAlinanBelgeSiraNo: sonAlinanBelgeSiraNo,
            belgeTuru: 'FATURA'
        })
        if (gelenBelgeler.length == 0) {
            break
        }

        // 2. Her belgeyi sorgula
        for (let gelenBelge of gelenBelgeler) {
            //console.log(gelenBelge)
            //fs.writeFileSync(`${gelenBelge.belgeNo}.xml`, gelenBelge.belgeVerisi)
            let xmlObj = await Xml.parseStringPromise(gelenBelge.belgeVerisi)
            //console.log(xmlObj)

            sonAlinanBelgeSiraNo = gelenBelge.belgeSiraNo

            // Tedarikci bul, yoksa ekle
            let tedarikci = await TedarikciModel.findOne({ vergiNo: xmlObj.fatura.satici[0].vergiNo }).lean(true).exec()
            if (!tedarikci) {
                tedarikci = await new TedarikciModel({
                    vergiNo: xmlObj.fatura.satici[0].vergiNo ? xmlObj.fatura.satici[0].vergiNo[0] : '',
                    vergiDairesi: xmlObj.fatura.satici[0].vergiDairesi ? xmlObj.fatura.satici[0].vergiDairesi[0] : '',
                    adi: xmlObj.fatura.satici[0].unvan ? xmlObj.fatura.satici[0].unvan[0] : '',
                    ulke: xmlObj.fatura.satici[0].ulke ? xmlObj.fatura.satici[0].ulke[0] : '',
                    il: xmlObj.fatura.satici[0].sehir ? xmlObj.fatura.satici[0].sehir[0] : '',
                    ilce: xmlObj.fatura.satici[0].ilce ? xmlObj.fatura.satici[0].ilce[0] : '',
                    adres: xmlObj.fatura.satici[0].caddeSokak ? xmlObj.fatura.satici[0].caddeSokak[0] : '',
                    telefon: xmlObj.fatura.satici[0].tel ? xmlObj.fatura.satici[0].tel[0] : '',
                    mail: xmlObj.fatura.satici[0].eposta ? xmlObj.fatura.satici[0].eposta[0] : '',
                }).save()
            }






            let gelenFaturaModel = {
                tenantKey: tenant.key,
                key: uuid(),
                belgeNo: gelenBelge.belgeNo,
                belgeSiraNo: parseInt(gelenBelge.belgeSiraNo),
                belgeTarihi: gelenBelge.belgeTarihi,
                belgeTuru: gelenBelge.belgeTuru,
                ettn: gelenBelge.ettn,
                gonderenEtiket: gelenBelge.gonderenEtiket,
                gonderenVknTckn: gelenBelge.gonderenVknTckn,

                // Fatura XML detayindan gelen bilgiler
                faturaId: xmlObj.fatura.faturaId[0],
                faturaTarihi: xmlObj.fatura.faturaTarihi[0],
                faturaNo: xmlObj.fatura.faturaNo[0],
                faturaTipi: xmlObj.fatura.faturaTipi[0],
                faturaTuru: xmlObj.fatura.faturaTuru[0],
                paraBirimi: xmlObj.fatura.paraBirimi[0],

                saticiVergiNo: xmlObj.fatura.satici[0].vergiNo ? xmlObj.fatura.satici[0].vergiNo[0] : '',
                saticiVergiDairesi: xmlObj.fatura.satici[0].vergiDairesi ? xmlObj.fatura.satici[0].vergiDairesi[0] : '',
                saticiUnvan: xmlObj.fatura.satici[0].unvan ? xmlObj.fatura.satici[0].unvan[0] : '',
                saticiUlke: xmlObj.fatura.satici[0].ulke ? xmlObj.fatura.satici[0].ulke[0] : '',
                saticiSehir: xmlObj.fatura.satici[0].sehir ? xmlObj.fatura.satici[0].sehir[0] : '',
                saticiIlce: xmlObj.fatura.satici[0].ilce ? xmlObj.fatura.satici[0].ilce[0] : '',
                saticiCaddeSokak: xmlObj.fatura.satici[0].caddeSokak ? xmlObj.fatura.satici[0].caddeSokak[0] : '',
                saticiTel: xmlObj.fatura.satici[0].tel ? xmlObj.fatura.satici[0].tel[0] : '',
                saticiEposta: xmlObj.fatura.satici[0].eposta ? xmlObj.fatura.satici[0].eposta[0] : '',
                kalemler: xmlObj.fatura.faturaSatir.map(satir => {
                    return {
                        siraNo: satir.siraNo ? satir.siraNo[0] : '',
                        urunAdi: satir.urunAdi ? satir.urunAdi[0] : '',
                        birimKodu: satir.birimKodu ? satir.birimKodu[0] : '',
                        birimFiyat: parseFloat(satir.birimFiyat ? satir.birimFiyat[0] : 0),
                        miktar: parseInt(satir.miktar ? satir.miktar[0] : 0),
                        malHizmetMiktari: parseFloat(satir.malHizmetMiktari ? satir.malHizmetMiktari[0] : 0),
                        iskontoOrani: parseInt(satir.iskontoOrani ? satir.iskontoOrani[0] : 0),
                        iskontoTutari: parseFloat(satir.iskontoTutari ? satir.iskontoTutari[0] : 0),
                        vergiler: !satir.vergiler ? [] : satir.vergiler.map(v => {
                            return {
                                toplamVergiTutari: parseFloat(v.toplamVergiTutari ? v.toplamVergiTutari[0] : 0),
                                vergi: v.vergi.map(_v => {
                                    return {
                                        ad: _v.ad ? _v.ad[0] : '',
                                        kod: _v.kod ? _v.kod[0] : '',
                                        matrah: parseFloat(_v.matrah ? _v.matrah[0] : 0),
                                        oran: parseInt(_v.oran ? _v.oran[0] : 0),
                                        vergiTutari: parseFloat(_v.vergiTutari ? _v.vergiTutari[0] : 0)
                                    }
                                })
                            }
                        })
                    }
                }),

                toplamMalHizmetMiktari: parseFloat(xmlObj.fatura.toplamMalHizmetMiktari ? xmlObj.fatura.toplamMalHizmetMiktari[0] : 0),
                toplamIskontoTutari: parseFloat(xmlObj.fatura.toplamIskontoTutari ? xmlObj.fatura.toplamIskontoTutari[0] : 0),
                toplamArtirimTutari: parseFloat(xmlObj.fatura.toplamArtirimTutari ? xmlObj.fatura.toplamArtirimTutari[0] : 0),
                vergiHaricToplam: parseFloat(xmlObj.fatura.vergiHaricToplam ? xmlObj.fatura.vergiHaricToplam[0] : 0),
                vergiDahilTutar: parseFloat(xmlObj.fatura.vergiDahilTutar ? xmlObj.fatura.vergiDahilTutar[0] : 0),
                yuvarlamaTutari: parseFloat(xmlObj.fatura.yuvarlamaTutari ? xmlObj.fatura.yuvarlamaTutari[0] : 0),
                odenecekTutar: parseFloat(xmlObj.fatura.odenecekTutar ? xmlObj.fatura.odenecekTutar[0] : 0),

                vergiler: xmlObj.fatura.vergiler.map(vergi => {
                    return {
                        toplamVergiTutari: parseFloat(vergi.toplamVergiTutari[0]),
                        vergi: vergi.vergi.map(_v => {
                            return {
                                ad: _v.ad ? _v.ad[0] : '',
                                kod: _v.kod ? _v.kod[0] : '',
                                matrah: parseFloat(_v.matrah ? _v.matrah[0] : 0),
                                oran: parseInt(_v.oran ? _v.oran[0] : 0),
                                vergiTutari: parseFloat(_v.vergiTutari ? _v.vergiTutari[0] : 0)
                            }
                        })
                    }
                }),
                faturaNot: xmlObj.fatura.faturaNot ? xmlObj.fatura.faturaNot[0] : '',
                sorguTarihi: new Date(),

                odendi: false,
                odemeTarihi: null,
                odemePlanlandi: false,
                planlananOdemeTarihi: null,

                tedarikci: tedarikci,
                aliciFirma: tenant
            }

            gelenFaturaModel.belgeVerisiXML = gelenBelge.belgeVerisi

            //console.log(gelenFaturaModel)

            // 3. Her belgenin PDF'ini al
            let zipData = await GelenBelgeleriIndir(client, {
                vergiTcKimlikNo: tenant.vkn, // '0710426308',
                ettnler: gelenBelge.ettn,
                belgeTuru: 'FATURA',
                belgeFormati: 'PDF'
            })

            let buf = Buffer.from(zipData, 'base64')
            var zip = new AdmZip(buf);
            var zipEntries = zip.getEntries();
            for (let zipEntry of zipEntries) {
                gelenFaturaModel.faturaPDF = zipEntry.getData().toString('base64')
                //fs.writeFileSync(zipEntry.entryName, zipEntry.getData())
            }

            //console.log(zipEntries)

            // Kaydet
            await new GelenFaturaModel(gelenFaturaModel).save()
            console.log(gelenFaturaModel.belgeSiraNo)
        }
    } while (true) // TODO
    //await initMongo.disconnect()
}


//Process({ tenantKey: 'EXPERTO' }).then(e => {
//    console.log('OK')
//})




const TumGelenFaturalariAl = async () => {
    await initMongo.connect(process.env.MONGO_URI)

    let tenants = await TenantModel.find().exec()
    for (let tenant of tenants) {
        await Process({ tenantKey: tenant.key })
    }
    await initMongo.disconnect()
}

TumGelenFaturalariAl()