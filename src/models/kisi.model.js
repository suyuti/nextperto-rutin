const mongoose = require('mongoose');
//var diffHistory = require('mongoose-diff-history/diffHistory')
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key       : { type: String },
    vfirma    : String,
    adi       : { type: String, required: false  },
    soyadi    : { type: String, required: false  },
    unvani    : { type: String, required: false  },
    mail      : { type: String, required: false  },
    cinsiyet  : { type: String, required: false  },
    cepTelefon: { type: String, required: false  },
    isTelefon : { type: String, required: false  },
    dahili    : { type: String, required: false  },
    departman : { type: String, required: false  },
    tckn      : { type: String, required: false  },
    dogumGunu : { type: Date,   required: false  },
    active    : { type: Boolean },
    oldId     : String
},{
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
})

//SchemaModel.plugin(diffHistory.plugin)

SchemaModel.virtual('firma', {
    ref         : 'Firma',
    localField  : 'vfirma',
    foreignField: 'key',
    justOne     : true
})

module.exports = mongoose.models.Kisi || mongoose.model("Kisi", SchemaModel);
