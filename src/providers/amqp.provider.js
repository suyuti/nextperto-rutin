const amqplib = require("amqplib/callback_api");

const AMQP_URL      = process.env.AMQP_URL
const MAIL_QUEUE    = process.env.MAIL_QUEUE
//const EVENT_QUEUE   = process.env.EVENT_QUEUE
//const FATURA_QUEUE  = process.env.QUEUE_FATURAGONDER


const send = async (queue, message) => {
    amqplib.connect(AMQP_URL, (err, connection) => {
        if (err) {
            console.log(`${AMQP_URL} not connected`)
            console.log(err)
            return
        }
        connection.createChannel((err, channel) => {
            if (err) {
                console.log(`CreateChannel Error`)
                console.log(err)
                return
            }
            channel.assertQueue(queue, { durable: true }, (err) => {
                if (err) {
                    console.log(`${queue} assertQueue error`)
                    console.log(err)
                    return
                }

                let sender = (content) => {
                    let sent = channel.sendToQueue(queue,
                        Buffer.from(JSON.stringify(content)), 
                        {
                            persistent: true,
                            contentType: 'application/json'
                        }
                    )

                    if (sent) {
                        console.log('Fatura gonderildi')
                        return channel.close(() => connection.close())
                    }
                    else {
                        channel.once('drain', () => next())
                    }
                }

                sender(message)
            })
        })
    })
}

exports.sendMail = async (mail) => {
    return send(MAIL_QUEUE, mail)
}

